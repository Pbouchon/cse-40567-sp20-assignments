// Patrick Bouchon, HW06, Security
#include <iostream>
#include<fstream>
#include <string>
#include <crafter.h>
#include <crafter/Utils/TCPConnection.h>
//code example taken from https://github.com/pellegre/libcrafter-examples/blob/master/TCPTraceroute/main.cpp

/* Collapse namespaces */
using namespace std;
using namespace Crafter;

int main(int argc, char* argv[]) {
  /* Command Line Args */
  //  the network interface, the target IP, the starting port number, the ending port number
  if(argc != 5){
    printf("Usage: %s networkinterface targetIP startPortNum endPortNum\n",argv[0]);
    exit(1);
  }
	InitCrafter();
	/* Set the interface */
	string iface = argv[1];

	/* ----------- Host and traceroute data --------- */

	/* This is the IP we want to scan */
	string DstIP = argv[2];
	cout << "Scanning " << DstIP << "\n";

	/*  Set the destination port for the TCP packets */
	int port_number_start = atoi(argv[3]);
  	int port_number_end = atoi(argv[4]);
  	int port_number = port_number_start;

	string MyIP = GetMyIP(iface);

	IP ip_header;

	cout << "My IP is " << MyIP << "\n";

	ip_header.SetSourceIP(MyIP);
	ip_header.SetDestinationIP(DstIP);

	IPOption security;
	security.SetOption(8);
	security.SetPayload("\x1\x1");

	TCP tcp_header;

	for(port_number = port_number_start; port_number <= port_number_end; port_number++){

		tcp_header.SetSrcPort(RNG16());
		tcp_header.SetDstPort(port_number);
		tcp_header.SetSeqNumber(RNG32());
		tcp_header.SetFlags(TCP::SYN);

		RawLayer raw_header;
		raw_header.SetPayload("SomeTCPPayload\n");

		Packet packet = ip_header / security / security / security / tcp_header / TCPOption::NOP / TCPOption::NOP / TCPOption::NOP / TCPOption::EOL / raw_header;

		Packet* pck_rcv = packet.SendRecv(iface,0.1,3);
		//packet.Print();

		if(pck_rcv){
			//pck_rcv->Print();

			TCP* rcv_tcp = pck_rcv->GetLayer<TCP>();
			if(rcv_tcp->GetACK() && rcv_tcp->GetSYN()){
				printf("Port %d open  \n",port_number);
			}
			else{
				printf("Port %d closed\n",port_number);
			}
		}
		else {
			printf("No response on port %d\n",port_number);
		}
	}
	return 0;
}
